
const MirrazzLyrics=(function () {
    // this array contains all the songs
    /**
     * @type {{
       title: string,
       artist: string,
       album: string
       lyrics: {
           type: "word" | "syllable"
           text: string
           time: number
       }[]|{
           type: "linebreak"
       }[]|{
           type: "end"
           time: number
       }[]
      }[]}
     */
    var songs=[
        {
            title: "The Kahoot Rap (Kahoot Star)",
            artist: "Kyle Exum and Ricky Desktop",
            album: "The Kahoot Rap (Kahoot Star)",
            lyrics: [
                {
                    type: "word",
                    text: "Top",
                    time: 3.34
                },
                {
                    type: "word",
                    text: "of",
                    time:3.44
                },
                {
                    type: "word",
                    text: "the",
                    time: 3.55
                },
                {
                    type: "word",
                    text: "game",
                    time: 3.65
                },
                {
                    type: "word",
                    text: "I'm",
                    time: 3.86
                },
                {
                    type: "word",
                    text: "a",
                    time: 3.97
                },
                {
                    type: "syllable",
                    text: "Ka",
                    time: 4.09
                }, 
                {
                    type: "word",
                    text: "hoot",
                    time: 4.27
                },
                {
                    type: "word",
                    text: "star",
                    time: 4.55
                },
                {
                    type: "syllable",
                    text: "(who?)",
                    time: 4.84
                },
                {
                    type: "linebreak"
                },
                {
                    type: "word",
                    text: "ain't",
                    time: 5.08
                },
                {
                    type: "word",
                    text: "no",
                    time: 5.14
                },
                {
                    type:"syllable",
                    text:"hoop",
                    time: 5.37
                },
                {
                    type: "word",
                    text: "la",
                    time: 5.61
                },
                {
                    type: "word",
                    text: "to",
                    time: 5.88
                },
                {
                    type: "word",
                    text: "it",
                    time: 5.99
                },
                {
                    type: 'linebreak'
                },
                {
                    type: "syllable",
                    text: "sec",
                    time: 6.15
                },
                {
                    type: "word",
                    text: "ond",
                    time: 6.27
                },
                {
                    type: "syllable",
                    text: 'guess',
                    time: 6.34
                },
                {
                    type: "word",
                    text: "in'?",
                    time: 6.53
                },
                {
                    type: "word",
                    text: "y'all",
                    time:6.68
                },
                {
                    type: "syllable",
                    text: "bue",
                    time: 6.85
                },
                {
                    type: "word",
                    text: "nos",
                    time: 6.97
                },
                {
                    type: "syllable",
                    text: "noch",
                    time: 7.25
                },
                {
                    type: "word",
                    text: "es",
                    time: 7.36
                },
                {
                    type: "linebreak"
                },
                {
                    type: "word",
                    text: "I",
                    time: 7.59
                },
                {
                    type: "word",
                    text: "can't",
                    time: 7.64
                },
                {
                    type: "word",
                    text: "be",
                    time: 7.83
                },
                {
                    type: "syllable",
                    text: "snooz",
                    time: 8.02
                },
                {
                    type: "word",
                    text: "in'",
                    time: 8.14
                },
                {
                    type: "linebreak",
                },
                {
                    type: "word",
                    text: "got",
                    time: 8.29
                },
                {
                    type: "word",
                    text: "the",
                    time: 8.39
                },
                {
                    type: "word",
                    text: "codes",
                    time: 8.57
                },
                {
                    type: "word",
                    text: "to",
                    time: 8.73
                },
                {
                    type: "word",
                    text: "the",
                    time: 8.96
                },
                {
                    type: "word",
                    text: 'game',
                    time: 9.04
                },
                {
                    type: 'linebreak'
                },
                {
                    type: "word",
                    text: 'made',
                    time: 9.25
                },
                {
                    type: "word",
                    text: 'my',
                    time: 9.45
                },
                {
                    type: "word",
                    text: 'name',
                    time: 9.61
                },
                {
                    type: 'word',
                    text: "make",
                    time: 9.93
                },
                {
                    type: 'word',
                    text: "a",
                    time: 10
                },
                {
                    type: 'word',
                    text: 'bang',
                    time: 10.17
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: 'red',
                    time: 10.45
                },
                {
                    type: 'word',
                    text: 'and',
                    time: 10.53
                },
                {
                    type: 'word',
                    text: 'blue,',
                    time: 10.64
                },
                {
                    type: 'word',
                    text: 'sus',
                    time: 10.84
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: 'like',
                    time: 11.09
                },
                {
                    type: 'syllable',
                    text: 'Am',
                    time: 11.24
                },
                {
                    type: 'word',
                    text: "ong",
                    time: 11.37
                },
                {
                    type: 'word',
                    text: "Us",
                    time: 11.6
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: "I",
                    time: 11.92
                },
                {
                    type: 'word',
                    text: "can't",
                    time: 12.03
                },
                {
                    type: 'word',
                    text: "be",
                    time: 12.14
                },
                {
                    type: 'syllable',
                    text: "choo",
                    time: 12.29
                },
                {
                    type: 'word',
                    text: "sin'",
                    time: 12.38
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: "I'm",
                    time: 12.56
                },
                {
                    type: 'word',
                    text: "a",
                    time: 12.64
                },
                {
                    type: 'word',
                    text: "quick",
                    time: 12.80
                },
                {
                    type: 'word',
                    text: 'card',
                    time: 13.11
                },
                {
                    type: 'syllable',
                    text: 'click',
                    time: 13.37
                },
                {
                    type: 'word',
                    text: "in'",
                    time:13.49
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'syllable',
                    text: "buzz",
                    time: 13.61
                },
                {
                    type: 'syllable',
                    text: "er",
                    time: 13.73
                },
                {
                    type: 'syllable',
                    text: "-beat",
                    time: 13.91
                },
                {
                    type: 'word',
                    text: 'er',
                    time: 14.04
                },
                {
                    type: 'word',
                    text: "with",
                    time: 14.13
                },
                {
                    type: 'word',
                    text: "the",
                    time: 14.28
                },
                {
                    type: 'word',
                    text: "top",
                    time: 14.45
                },
                {
                    type: 'word',
                    text: "spot",
                    time: 14.66
                },
                {
                    type: "linebreak"
                },
                {
                    type: 'word',
                    text: 'green',
                    time: 14.99
                },
                {
                    type: 'word',
                    text: 'I',
                    time: 15.25
                },
                {
                    type: 'word',
                    text: 'see,',
                    time: 15.47
                },
                {
                    type: 'word',
                    text: 'if',
                    time: 15.64
                },
                {
                    type: 'linebreak',
                },
                {
                    type: 'word',
                    text: 'you',
                    time: 15.71
                },
                {
                    type: 'word',
                    text: "ain't",
                    time: 15.85
                },
                {
                    type: 'word',
                    text: "me",
                    time: 16.01
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: "you",
                    time: 16.12
                },
                {
                    type: 'word',
                    text: 'just',
                    time: 16.29
                },
                {
                    type: 'word',
                    text: 'a',
                    time: 16.44
                },
                {
                    type: 'syllable',
                    text: 'knock',
                    time: 16.59
                },
                {
                    type: 'word',
                    text: 'off',
                    time: 16.86
                },
                {
                    type: 'linebreak',
                },
                {
                    type: 'word',
                    text: 'grades',
                    time: 17.14
                },
                {
                    type: 'word',
                    text: 'got',
                    time: 17.36
                },
                {
                    type: 'syllable',
                    text: 'CO',
                    time: 17.59
                },
                {
                    type: 'word',
                    text: 'VID',
                    time: 17.75
                },
                {
                    type: 'word',
                    text: 'but',
                    time: 17.92
                },
                {
                    type: 'syllable',
                    text: 'in',
                    time: 18.03
                },
                {
                    type: 'word',
                    text: '-game',
                    time: 18.15
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: "I'm",
                    time: 18.29
                },
                {
                    type: 'syllable',
                    text: "Jim",
                    time: 18.42,
                },
                {
                    type: 'word',
                    text: "my",
                    time: 18.57
                },
                {
                    type: "syllable",
                    text: "Neu",
                    time: 18.75
                },
                {
                    type: 'word',
                    text: "tron",
                    time: 18.96
                },
                {
                    type: 'linebreak',
                },
                {
                    type: 'syllable',
                    text: "play",
                    time: 19.25
                },
                {
                    type: 'word',
                    text: 'er',
                    time: 19.45
                },
                {
                    type: 'word',
                    text: 'of',
                    time: 19.52
                },
                {
                    type: 'word',
                    text: 'the',
                    time: 19.66
                },
                {
                    type: 'word',
                    text: 'year',
                    time: 19.75
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: 'just',
                    time: 19.89
                },
                {
                    type: 'word',
                    text: 'like',
                    time: 20.03
                },
                {
                    type: 'word',
                    text: 'an',
                    time: 20.21
                },
                {
                    type: 'word',
                    text: 'ex',
                    time: 20.34
                },
                {
                    type: 'word',
                    text: 'I',
                    time: 20.44
                },
                {
                    type: 'syllable',
                    text: 'ne',
                    time: 20.58
                },
                {
                    type: 'word',
                    text: 'ver',
                    time: 20.71
                },
                {
                    type: 'word',
                    text: 'miss',
                    time: 20.83
                },
                {
                    type: 'linebreak'
                },
                {
                    type: 'word',
                    text: 'nah',
                    time: 21.13
                },
                {
                    type: 'word',
                    text: 'nah',
                    time: 21.37
                },
                {
                    type: 'end',
                    time: 125
                }
            ]
        }
    ];
    // Find a song based on the title artist and album
    /**
     * Find a song based on the title, artist, and album
     * @param title {string} - The title of the song
     * @param artist {string} - The artist(s) that play the song
     * @param album {string} - The name of the album the song is in
     */
    var findSong=function(title,artist,album) {
        for(var i=0;i<songs.length;i++) {
            if(songs[i].title===title&&songs[i].artist===artist&&songs[i].album===album) {
                return songs[i]
            }
        }
        // If a match isn't found, return null
        return null
    }
    /**
     * @param song{{
       title: string,
       artist: string,
       album: string
       lyrics: {
           type: "word" | "syllable"
           text: string
           time: number
       }[]|{
           type: "linebreak"
       }[]|{
           type: "end"
           time: number
       }[]
      }} The song
     * @param time {number}
     */
    var matchTime=function(song,time) {
        var isLit=true;
        var lyrics=song.lyrics;
        var next="";
        for(var i=0;i<lyrics.length;i++) {
            var lyr=lyrics[i]
            if((lyr.type != "linebreak") && isLit && (lyr.type != "end")) {
                if(time <= lyr.time) {
                    isLit=false;
                    next+="?";
                }
            }
            if(lyr.type=="word") {
                next+=encodeURIComponent(lyr.text)+"%20";
            } else if(lyr.type=="syllable") {
                next+=encodeURIComponent(lyr.text);
            } else if(lyr.type=="linebreak") {
                next+="%0A";
            } else if(lyr.type=="end"&&isLit) {
                next+="?";
                isLit=false;
            }
        }
        return next;
    };

    /**
     * @param song{{
       title: string,
       artist: string,
       album: string
       lyrics: {
           type: "word" | "syllable"
           text: string
           time: number
       }[]|{
           type: "linebreak"
       }[]|{
           type: "end"
           time: number
       }[]
      }} The song
     * @param time {number} The time in the song
     * @param tag {string} The element's HTML tag
     * @param properties {string?} The element's properties (eg class)
     */
    var matchedTimeToHTML=function(song,time,tag,properties) {
        var match=matchTime(song,time);
        if(match.indexOf("?")===-1) {
            return htmlEncode(decodeURIComponent(match))
        }
        var split=match.split("?");
        var litText=decodeURIComponent(split[0]);
        var dimText=decodeURIComponent(split[1]);
        var result="<"+tag;
        if(properties) { result += " "; result += properties;};
        result+=">";
        result+=htmlEncode(litText);
        result += ( "</" + tag + ">" );
        result+=htmlEncode(dimText);
        return result;
    };


    /**
     * Converts the stored lyrics and song data to a user-friendly-format.
     *  @param song{{
       title: string,
       artist: string,
       album: string
       lyrics: {
           type: "word" | "syllable" | "linebreak",
           text?: string,
           time?: number
       }[]
      }} The song
     */
    var makeRawSongDataUserFriendly=function (song) {
        var ufsong={
            /**
             * The title of the song.
             */
            title: song.title,
            /**
             * The artist or artists that sing the song.
             */
            artist: song.artist,
            /**
             * The name of the album the song is in.
             */
            album: song.album,
            /**
             * The raw unsynced lyrics to the song.
             */
            lyrics: "",
            /**
             * Sync lyrics to a specific time point.
             * @param time{number} The time point in milliseconds.
             */
            syncLyrics: function (time) {
                return matchTime(song,time)
            },
            /**
             * Sync lyrics to a specific time point and output in HTML.
             * @param time{number} The time point in milliseconds.
             * @param tag{string} The tag name of the element for active lyrics, eg `span`
             * @param prop{string?} Any properties for the element of active lyrics, eg `class="my-class"`
             */
            syncLyricsHTML:function(time,tag,prop) {
                return matchedTimeToHTML(song,time,tag,prop)
            },
            /**
             * Gets the non-user-friendly version of the song.
             */
            _getUnfriendLy: function () {
                return song
            }
        }
        for(var i=0;i<song.lyrics.length;i++) {
            if(song.lyrics[i].type==="syllable") {
                ufsong.lyrics+=song.lyrics[i].text;
            } else if(song.lyrics[i].type==="word") {
                ufsong.lyrics+=( song.lyrics[i].text + " " )
            } else if(song.lyrics[i].type==="linebreak") {
                ufsong.lyrics+="\n"
            }
        }
        return ufsong
    }


    /**
     * @param text {string} The text to encode.
     */
    // Used for HTML Output, simple utilities
    var htmlEncode=function(text) {
        var encoded="";
        // first every letter in the alphabet
        var allowedCharacters="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // convert to lowercase to prevent typing again
        allowedCharacters+=allowedCharacters.toLowerCase();
        // add basic symbols and numbers
        allowedCharacters+="0123456789_-!@#$%^()[]{}/\\|.,:;+=~";
        for(var i=0;i<text.length;i++) {
            if(allowedCharacters.indexOf(text[i])>-1) {
                encoded+=text[i];
            } else if(text[i]===" ") {
                // this is the equivalent of `&nbsp;` or non-breaking space
                encoded+="&#xA0;";
            } else if(text[i]==="\n") {
                // a new line will always be a break
                encoded+="<br />";
            } else {
                // hex is not required
                encoded+="&#"+text.charCodeAt(i)+";";
            }
        }
        return encoded;
    }

    // the actual API
    return {
        /**
         * Gets a song.
         * @param title{string} The full title of the song.
         * @param artist{string} The artist(s) who sing(s) the song.
         * @param album{string} The name of the album the song is in.
         */
        getSong: function(title,artist,album) {
            var song=findSong(title,artist,album);
            if (!song) { return null }
            return makeRawSongDataUserFriendly(song);
        },
        /**
         * Find a song by part of it's title.
         * @param searchQuery{string} The text to look for in the title.
         */
        searchSongByTitle: function (searchQuery) {
            var results=[];
            for(var i=0;i<songs.length;i++) {
                if(songs[i].title.indexOf(title)>-1) {
                    results.push(
                        makeRawSongDataUserFriendly(songs[i])
                    );
                }
            };
            return results
        }
    }
})()
